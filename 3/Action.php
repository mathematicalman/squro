<?php
require_once 'DatabaseClient.php';

switch ($_POST['action']) {
    case 'add':
        if(!isset($_POST['text']) || !isset($_POST['parentId'])) break;
        try{
            $id = DatabaseClient::getInstance()->addNode($_POST['text'], $_POST['parentId']);
            echo $id;
        } catch(DatabaseException $e){
            echo $e->getMessage();
        }
        break;
    case 'update':
        if(!isset($_POST['id']) || !isset($_POST['text'])) break;
        try{
            DatabaseClient::getInstance()->updateNode($_POST['id'], $_POST['text']);
        } catch(DatabaseException $e){
            echo $e->getMessage();
        }
        break;
    case 'move':
        if(!isset($_POST['id']) || !isset($_POST['parentId'])) break;
        try{
            DatabaseClient::getInstance()->moveNode($_POST['id'], $_POST['parentId']);
        } catch(DatabaseException $e){
            echo $e->getMessage();
        }
        break;
    case 'delete':
        if(!isset($_POST['id'])) break;
        try{
            DatabaseClient::getInstance()->deleteNode($_POST['id']);
        } catch(DatabaseException $e){
            echo $e->getMessage();
        }
        break;
    default:
        try{
            echo DatabaseClient::getInstance()->getTree();
        } catch(DatabaseException $e){
            echo $e->getMessage();
        }
        break;
}