<?php
require_once 'DatabaseException.php';

class DatabaseClient {
    private static $instance;
    private $handle;
    
    private function __construct() {
        $this->handle = new SQLite3("3.sqlite");
        if (!$this->handle) {
            throw new DatabaseException('Could not connect/create database file');
            exit();
        }
        do {
            @$query = $this->handle->exec("CREATE TABLE IF NOT EXISTS nodes("
                    . "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    . "text TEXT,"
                    . "parent_id INTEGER);"
                    . "INSERT OR IGNORE INTO nodes(id, text, parent_id) VALUES(1, 'root', 0);");
            if ($this->handle->lastErrorCode() == 5) {//Database is locked
                sleep(1);
                continue;
            } else if ($this->handle->lastErrorCode() != 0) {
                throw new DatabaseException('Could not create database');
                exit();
            }
        } while ($this->handle->lastErrorCode() == 5); //Database is locked
        return $this;
    }

    function __destruct() {
        $this->handle->close();
    }
    private function __clone()    { }
    private function __wakeup()   { }
    public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    public function getTree($parentId = 1){
        if(!$stmt = $this->handle->prepare('SELECT id, text FROM nodes WHERE parent_id=?;')) throw new DatabaseException("Could not prepeare");
        if(!$stmt->bindValue(1, $parentId, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
        if(!$result = $stmt->execute()) throw new DatabaseException("Could not execute");
        
        $str = "";
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $childTree = $this->getTree($row['id']);//contenteditable  
            $str .= '<li class="draggable"><span contenteditable id=' . $row['id'] . ' class="droppable">' . htmlspecialchars($row['text']) . '</span><div class='
                    . (($childTree == "") ? 'triangle-none' : 'triangle-down')
                    .' ></div><ul>'
                    . $childTree
                    . '</ul></li>';
        }    
        return $str;
    }
    
    public function addNode($text, $parentId){
        $this->handle->exec("BEGIN TRANSACTION;");
        if(!$stmt = $this->handle->prepare('INSERT INTO nodes(text, parent_id) VALUES(:text, :parent_id);')) throw new DatabaseException("Could not prepeare");
        if(!$stmt->bindValue(':text', $text, SQLITE3_TEXT)) throw new DatabaseException("Could not bind value");
        if(!$stmt->bindValue(':parent_id', $parentId, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
        if(!$result = $stmt->execute()) throw new DatabaseException("Could not execute");
        $insertId = $this->handle->querySingle('SELECT MAX(id) FROM nodes;');
        $this->handle->exec("COMMIT;");
        echo $insertId;
    }
    
    public function updateNode($id, $text){
        if(!$stmt = $this->handle->prepare('UPDATE nodes SET text=:text WHERE id=:id;')) throw new DatabaseException("Could not prepeare");
        if(!$stmt->bindValue(':text', $text, SQLITE3_TEXT)) throw new DatabaseException("Could not bind value");
        if(!$stmt->bindValue(':id', $id, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
        if(!$result = $stmt->execute()) throw new DatabaseException("Could not execute");
    }
    
    public function moveNode($id, $newParentId){
        if(!$stmt = $this->handle->prepare('UPDATE nodes SET parent_id=:parent_id WHERE id=:id;')) throw new DatabaseException("Could not prepeare");
        if(!$stmt->bindValue(':parent_id', $newParentId, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
        if(!$stmt->bindValue(':id', $id, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
        if(!$result = $stmt->execute()) throw new DatabaseException("Could not execute");
    }
    
    public function deleteNode($id) {
        //Начиная с версии 3.8.3 http://www.sqlite.org/releaselog/3_8_3.html можно пользоваться рекурсией 
        // и следующий код в комментариях /**/ полностью заменил бы нижеприведенный
        /* if(!$stmt = $this->handle->prepare(''
          . 'WITH RECURSIVE cnt(x) AS '
          . '(VALUES(:id) UNION SELECT id FROM nodes, cnt WHERE nodes.parent_id = cnt.x) '
          . 'DELETE FROM nodes WHERE id IN (SELECT DISTINCT x FROM cnt);'))
          throw new DatabaseException("Could not prepeare");
          if(!$stmt->bindValue(':id', $id, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
          if(!$result = $stmt->execute()) throw new DatabaseException("Could not execute"); */
        $ids = array($id);
        if (!$stmt = $this->handle->prepare('SELECT id FROM nodes WHERE parent_id=:id')) throw new DatabaseException("Could not prepeare");
        if (!$stmt->bindParam(':id', $id, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
        for (
        $parentIds = array($id),
        $childIds = array(); count($parentIds) > 0; $parentIds = $childIds,
                $childIds = array(),
                $parentIds ? $ids = array_merge($ids, $parentIds) : $ids = $ids) {
            foreach ($parentIds as $id) {
                if (!$result = $stmt->execute()) throw new DatabaseException("Could not execute");
                while ($row = $result->fetchArray(SQLITE3_NUM)) {
                    array_push($childIds, $row[0]);
                }
                if (!$stmt->reset()) throw new DatabaseException("Could not reset");
            }
        }
        $ids = array_unique($ids);
        if (!$stmt->clear()) throw new DatabaseException("Could not clear");
        if (!$stmt = $this->handle->prepare('DELETE FROM nodes WHERE id=:id')) throw new DatabaseException("Could not prepeare");
        if (!$stmt->bindParam(':id', $id, SQLITE3_INTEGER)) throw new DatabaseException("Could not bind value");
        foreach ($ids as $id) {
            if (!$stmt->execute()) throw new DatabaseException("Could not execute");
            if (!$stmt->reset()) throw new DatabaseException("Could not reset");
        }
    }
}