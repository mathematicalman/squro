$(document).ready(function() {
    
//Drag and drop
    draggable_options = {
        helper: "clone",
        start: function(event, ui) {
            //console.log("start dragging " + $(this).find("span").attr("id"));
            if ($(this).parent("ul").children().length > 2) {
                $(this).parent("ul").prev("div").attr("class", "triangle-down");
            } else {
                $(this).parent("ul").prev("div").attr("class", "triangle-none");
            }
        },
        stop: function(event, ui) {
            //console.log("stop dragging " + $(this).find("span").attr("id"));
            if ($(this).parent("ul").children().length > 0) {
                $(this).parent("ul").prev("div").attr("class", "triangle-down");
            } else {
                $(this).parent("ul").prev("div").attr("class", "triangle-none");
            }
        }
    };

    droppable_options = {
        drop: function(event, ui) {
            //console.log("dropping " + $(this).attr("id"));
            $.ajax({
                //async: false,
                type: "POST",
                url: "http://squro.com/3/Action.php",
                data: {action: "move", id: $(ui.draggable).find("span").attr("id"), parentId: $(this).attr("id")},
                success: function(msg) {
                    if (msg !== "")
                        alert(msg);
                }
            });   
            $(ui.draggable).appendTo($(this).next("div").next("ul"));
            $(this).next("div").attr("class", "triangle-down");
        }
    };

    $("div.tree ul li.draggable").draggable(draggable_options);
    $("div.tree span.droppable").droppable(droppable_options);
    
    $("div.tree").on('click', 'li.draggable', function() {
        $(this).draggable("option", "disabled", true);
        $(this).find("span").attr('contenteditable', 'true');
    });
    
    $("div.tree").on('blur', 'li.draggable', function() {
        $(this).draggable('option', 'disabled', false);
        $(this).find("span").attr('contenteditable', 'false');
    });
    
    var currentElement;//Current element
    $("div.tree").on('focusin', 'span', function(event) {
        currentElement = $(this);
    });
    
//Updating span value
    $("div.tree").on('focusout', 'span', function(event) {
        $.ajax({
            //async: false,
            type: "POST",
            url: "http://squro.com/3/Action.php",
            data: {action: "update", id: currentElement.attr("id"), text: currentElement.text()},
            success: function(msg) {
                if (msg !== "") {
                    alert(msg);
                }
            }
        });

    });
    
 //Collapse/ellapse tree
    $("div.tree").on('click', 'div', function() {
        if ($(this).attr("class") === "triangle-right") {
            $(this).next("ul").show();
            $(this).attr("class", "triangle-down");
        } else {
            $(this).next("ul").hide();//.css("border", "1px solid red");
            $(this).attr("class", "triangle-right");

        }
    });
 
//Context menu 
    $("div.tree").on('contextmenu', 'span', function(event) {
        currentElement = $(this);
        $('#context-menu').css({left: event.pageX + 'px', top: event.pageY + 'px', display: 'block'});
        if (currentElement.attr("id") === "1")
            $('#context-menu-delete').hide();
        else
            $('#context-menu-delete').show();
        return false;
    });

    $(document).on("click", function() {
        $('#context-menu').css('display', 'none');
    });  

    $('#context-menu-add').on("click", function() {
        var spanId;
        var thisCurrentElement = currentElement;
        $.ajax({
            //async: false,
            type: "POST",
            url: "http://squro.com/3/Action.php",
            data: {action: "add", text: "New node", parentId: thisCurrentElement.attr("id")},
            success: function(msg) {
                spanId = parseInt(msg);
                if (isNaN(spanId))
                    alert(msg);
                else {
                    thisCurrentElement.next("div").attr('class', 'triangle-down');
                    thisCurrentElement.next("div").next("ul").show();
                    var $appendNode = $("<li class=\"draggable\"><span contenteditable id=\"" + spanId + "\" class=\"droppable\">New node</span><div class=\"triangle-none\"></div><ul></ul></li>");
                    thisCurrentElement.next("div").next("ul").append($appendNode);
                    $appendNode.draggable(draggable_options);
                    $appendNode.find("span").droppable(droppable_options).focus();
                }
            }
        });
    });

    $('#context-menu-delete').on("click", function() {
        var error = false;
        $.ajax({
            async: false,
            type: "POST",
            url: "http://squro.com/3/Action.php",
            data: {action: "delete", id: currentElement.attr("id")},
            success: function(msg) {
                if (msg !== "") {
                    error = true;
                    alert(msg);
                }
            }
        });
        if (error)
            return;
        if (currentElement.parent("li").parent("ul").children().length === 1) {
            currentElement.parent("li").parent("ul").prev("div").attr("class", "triangle-none");
        }
        currentElement.parent("li").remove();
    });
});