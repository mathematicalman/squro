<?php

function relativeToAbsoluteUrl($relative, $base = "http://usr:pss@example.com:81/mypath/myfile.html?a=b&b[]=2&b[]=3#myfragment") {
    $baseUrl = parse_url($base);
    $relativeUrl = parse_url($relative);
    $url = "";
    $url .= isset($relativeUrl['scheme']) ? $relativeUrl['scheme'] : $baseUrl['scheme'];
    $url .= "://";
    $url .= isset($relativeUrl['user']) ? $relativeUrl['user'] : '';
    $url .= isset($relativeUrl['pass']) ? ':' . $relativeUrl['pass'] : '';
    $url .= isset($relativeUrl['user']) || isset($relativeUrl['pass']) ? '@' : '';
    $url .= isset($relativeUrl['host']) ? $relativeUrl['host'] : $baseUrl['host'];
    $url .= isset($relativeUrl['port']) ? ':' . $relativeUrl['port'] : '';

    if (isset($relativeUrl['path'])) {
        $path;
        if ($relativeUrl['path'][0] == "/")//ссылка относительно корня сайта
            $path = $relativeUrl['path'];
        else //ссылка относительно текущей директории
            $path = $baseUrl['path'] . '/' . $relativeUrl['path'];
        //Заменяем все // на /
        $path = preg_replace('/\/\//', '/', $path);
        echo '<br/>' . $path . '<br/>';
        //Заменяем все пути типа a/b/../c на a/c
        while (preg_match('/\/[^\.\/]+\.?[^\.\/]*\/\.\.\//', $path)) {
            $path = preg_replace('/\/[^\.\/]+\.?[^\.\/]*\/\.\.\//', '/', $path);
        }
        //Убираем все оставшиеся ../ так как это уже выход из корня сайта
        $path = preg_replace('/\/\.\.\//', '/', $path);
        $url .= $path;
    }

    $url .= isset($relativeUrl['query']) ? '?' . $relativeUrl['query'] : '';
    $url .= isset($relativeUrl['fragment']) ? '#' . $relativeUrl['fragment'] : '';
    return $url;
}

$testArray = array(
    "http://usr:pss@example.com:81/mypath/myfile.html?a=b&b[]=2&b[]=3#myfragment",
    "http://usr@example.com:81/mypath/myfile.html?a=b&b[]=2&b[]=3#myfragment",
    "http://example.com:81/mypath/myfile.html?a=b&b[]=2&b[]=3#myfragment",
    "http://example.com/mypath/myfile.html?a=b&b[]=2&b[]=3#myfragment",
    "http://example.com/mypath/myfile.html?a=b&b[]=2&b[]=3",
    "http://example.com/mypath/myfile.html#myfragment",
    "http://example.com/mypath/myfile.html",
    "http://example.com/mypath/",
    "http://example.com/mypath",
    "http://example.com/",
    "http://example.com",
    "some_document.html",
    "some document.html",
    "../some_document.html",
    "../some document.html",
    "../../some_document.html",
    "../../some document.html",
    "some_folder/some document.html",
    "some_folder1/some_folder2/some document.html",
    "/some_folder/",
    "/a/",
    "/a",
    "/a/b/c",
    "/a/../b/c",
    "/a/b/../../c",
    "/a/b/c/../../",
    "/a/b/c/../../../../",
    "../a/b/c/../",
    "a/b/c/../../"
);

foreach ($testArray as $value) {
    echo $value . " => " . relativeToAbsoluteUrl($value) . "<br/>";
}