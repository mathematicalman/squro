<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="description" content="Squro test task">
        <meta name="keywords" content="Squro, test, task, Тестовое, задание">
        <link rel="stylesheet" href="3/style.css">
        <script src="3/jquery-1.11.1.min.js"></script>
        <script src="3/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <script src="3/script.js"></script>
        <title>Тестовое задание Squro</title>
    </head>
    <body>
        <h1>Тестовое задание Squro</h1>
        <h2>
            Редактирование элемента - нажать левую кнопку мыши (сохраняется в базе после потери фокуса)<br/>
            Перетаскивание элемента - зажать левую кнопку мыши (сохраняется в базе сразу же)<br/>
            Контекстное меню - правая кнопка мыши<br/>
            Свернуть/развернуть - кликнуть по треугольнику<br/>
            В Chrome у новых элементов съезжает треугольник(в Firefox и IE все нормально отображается)
        </h2>
        <div id="context-menu">
                <div id="context-menu-add">Добавить</div>
                <div id="context-menu-delete">Удалить</div>
        </div>
        <div class="tree">
            <ul>
                <li>
                    <span id="1" class="droppable">root</span><div class="triangle-down"></div><ul>
                        <?php
                        error_reporting(E_ALL);
                        echo file_get_contents('http://www.squro.com/3/Action.php');
                        ?>
                    </ul>
                </li>            
            </ul>
        </div>
    </body>
</html>