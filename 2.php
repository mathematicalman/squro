<?php
error_reporting(E_ALL);

function relativeToAbsoluteUrl($relative, $base) {
    $baseUrl = parse_url($base);
    $relativeUrl = parse_url($relative);
    $url = "";
    $url .= isset($relativeUrl['scheme']) ? $relativeUrl['scheme'] : $baseUrl['scheme'];
    $url .= "://";
    $url .= isset($relativeUrl['user']) ? $relativeUrl['user'] : '';
    $url .= isset($relativeUrl['pass']) ? ':' . $relativeUrl['pass'] : '';
    $url .= isset($relativeUrl['user']) || isset($relativeUrl['pass']) ? '@' : '';
    $url .= isset($relativeUrl['host']) ? $relativeUrl['host'] : $baseUrl['host'];
    $url .= isset($relativeUrl['port']) ? ':' . $relativeUrl['port'] : '';

    if (isset($relativeUrl['path'])) {
        $path;
        if ($relativeUrl['path'][0] == "/")//ссылка относительно корня сайта
            $path = $relativeUrl['path'];
        else //ссылка относительно текущей директории
            $path = $baseUrl['path'] . '/' . $relativeUrl['path'];
        //Заменяем все // на /
        $path = preg_replace('/\/\//', '/', $path);
        //Заменяем все пути типа a/b/../c на a/c
        while (preg_match('/\/[^\.\/]+\.?[^\.\/]*\/\.\.\//', $path)) {
            $path = preg_replace('/\/[^\.\/]+\.?[^\.\/]*\/\.\.\//', '/', $path);
        }
        //Убираем все оставшиеся ../ так как это уже выход из корня сайта
        $path = preg_replace('/\/\.\.\//', '/', $path);
        $url .= $path;
    }

    $url .= isset($relativeUrl['query']) ? '?' . $relativeUrl['query'] : '';
    $url .= isset($relativeUrl['fragment']) ? '#' . $relativeUrl['fragment'] : '';
    return $url;
}

$url = $argv[1];
$dom = new DOMDocument();
@$content = file_get_contents($url);
if($content === false) {echo "Incorrect URL!"; die();}
$libxml_previous_state = libxml_use_internal_errors(true);
$dom->loadHTML($content);
libxml_clear_errors();
libxml_use_internal_errors($libxml_previous_state);

$nodes = $dom->getElementsByTagName('a');
$hrefs = array();
foreach ($nodes as $node) {
    $hrefs[] = relativeToAbsoluteUrl($node->getAttribute('href'), $url);
}
echo 'Number of hrefs: ' . count($hrefs) . "\n";
$hrefs = array_unique($hrefs);
echo 'Number of unique hrefs: ' . count($hrefs) . "\n";
foreach ($hrefs as $href) {
    echo $href . "\n";
}